//
//  ContentView.swift
//  KukumberStorybook
//
//  Created by Alexander Ivashchenko on 06.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import SwiftUI
import Rswift

struct ContentView: View {
  let sections: [CellSection] = CellsConstructor().constructTotal()

  var body: some View {
    NavigationView {
      List {
        ForEach(sections) { section in
          Section(header: Text(section.title)) {
            ForEach(section.cells) { cell in
              VStack {
                NavigationLink(destination: DetailsContentView(
                  view: cell.view,
                  props: cell.props,
                  size: cell.size)
                ) {
                    Text(cell.state)
                }
              }
            }
          }
        }
      }
      .listStyle(GroupedListStyle())
      .navigationBarTitle(Text("Storybook"))
    }
    .accentColor(Color(R.color.tint()!))
  }
}

struct DetailsContentView: View {
  let view: PropsRenderable
  let props: RenderableProps
  let size: CGSize?

  var body: some View {
    if let view = view as? UIView & PropsRenderable {
      return AnyView(CustomViewDetailsContentView(view: view, props: props, size: size))
    } else if let view = view as? UIViewController & PropsRenderable {
      return AnyView(ScreenDetailsContentView(view: view, props: props))
    } else {
      return AnyView(EmptyView())
    }
  }
}

struct CustomViewDetailsContentView: View {
  let view: UIView & PropsRenderable
  let props: RenderableProps
  let size: CGSize?

  var body: some View {
    if let size = size {
      return AnyView(CustomView(customView: view, props: props)
        .frame(width: size.width, height: size.height)
      )
    } else {
      return AnyView(CustomView(customView: view, props: props)
        .fixedSize()
      )
    }
  }
}

struct ScreenDetailsContentView: View {
  let view: UIViewController & PropsRenderable
  let props: RenderableProps

  var body: some View {
    Screen(viewController: view, props: props)
      .frame(minWidth: 0, minHeight: 0)
      .edgesIgnoringSafeArea(.all)
  }
}

struct CustomView: UIViewRepresentable {
  let customView: UIView & PropsRenderable
  let props: RenderableProps

  func makeUIView(context: Context) -> UIView {
    customView.render(props)
    return customView
  }


  func updateUIView(_ uiView: UIView, context: Context) {
  }
}

struct Screen: View, UIViewControllerRepresentable {
  let viewController: UIViewController & PropsRenderable
  let props: RenderableProps

  func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    viewController.render(props)
  }

  func makeUIViewController(context: Context) -> UIViewController {
    return viewController
  }
}

struct CellsConstructor {
  func constructHomeCells() -> [Cell] {
    return [
      .init(
        state: "Базовое состояние",
        view: MainHomeViewController(),
        props: MainHomeViewController.Props.basic,
        size: .zero)
    ]
  }

  func constructNotificationViews() -> [Cell] {
    return [
      .init(
        state: "Базовый текст",
        view: NotificationView(),
        props: NotificationView.Props.basic,
        size: .init(width: 343, height: 56)),
      .init(
        state: "Огромный текст",
        view: NotificationView(),
        props: NotificationView.Props.hugeText,
        size: .init(width: 343, height: 115))
    ]
  }

  func constructTreatmentCells() -> [Cell] {
    return [
      .init(
        state: "Базовое состояние",
        view: TreatmentViewController(),
        props: TreatmentViewController.Props.basic,
        size: .zero)
    ]
  }

  func constructResearchCells() -> [Cell] {
    return [
      .init(
        state: "Базовое состояние",
        view: ResearchesViewController(),
        props: ResearchesViewController.Props.basic,
        size: .zero)
    ]
  }

  func constructMedicationDetailsViews() -> [Cell] {
    return [
      .init(
        state: "Базовое состояние",
        view: MedicationDetailsViewController(),
        props: MedicationDetailsViewController.Props.basic,
        size: .zero)
    ]
  }

  func constructVaccinationDetailsViews() -> [Cell] {
    return [
      .init(
        state: "Базовое состояние",
        view: VaccinationDetailsViewController(),
        props: VaccinationDetailsViewController.Props.basic,
        size: .zero)
    ]
  }

  func constructProfileViews() -> [Cell] {
    return [
      .init(
        state: "Базовое состояние",
        view: ProfileViewController(),
        props: ProfileViewController.Props.basic,
        size: .zero)
    ]
  }

  func constructTotal() -> [CellSection] {
    return [
      .init(title: "Домашняя страница", cells: self.constructHomeCells()),
      .init(title: "Лечение", cells: self.constructTreatmentCells()),
      .init(title: "Исследования", cells: self.constructResearchCells()),
      .init(title: "Уведомление", cells: self.constructNotificationViews()),
      .init(title: "Детали лекарства", cells: self.constructMedicationDetailsViews()),
      .init(title: "Детали вакцинации", cells: self.constructVaccinationDetailsViews()),
      .init(title: "Профиль", cells: self.constructProfileViews())
    ]
  }
}

struct Cell: Identifiable {
  let id = UUID()

  let state: String
  let view: PropsRenderable
  let props: RenderableProps
  let size: CGSize?
}

struct CellSection: Identifiable {
  let id = UUID()

  let title: String
  let cells: [Cell]
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
      ContentView()
    }
}
