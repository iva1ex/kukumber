//
//  UIView+AutoLayoutExtensions.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 10.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

enum AutolayoutSide {
  case left
  case right
  case top
  case bottom
}

enum HorizontalSide {
  case left
  case right
}

enum VerticalSide {
  case top
  case bottom
}

enum PinType {
  case equal
  case greaterThanOrEqual
  case lessThanOrEqual
}

extension Array where Element == AutolayoutSide {
  static var all: [AutolayoutSide] {
    return [.left, .right, .top, .bottom]
  }
}

extension UIView {
  func pinToSides(_ sides: [AutolayoutSide], to view: UIView, withPadding padding: CGFloat = 0) {
    translatesAutoresizingMaskIntoConstraints = false
    sides.forEach { side in
      switch side {
      case .left: leftAnchor.constraint(equalTo: view.leftAnchor, constant: padding).isActive = true
      case .right: rightAnchor.constraint(equalTo: view.rightAnchor, constant: -padding).isActive = true
      case .top: topAnchor.constraint(equalTo: view.topAnchor, constant: padding).isActive = true
      case .bottom: bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -padding).isActive = true
      }
    }
  }

  // swiftlint:disable cyclomatic_complexity
  func pinHorizontally(
    from fromSide: HorizontalSide,
    to toSide: HorizontalSide,
    of view: UIView,
    pinType: PinType = .equal,
    withPadding padding: CGFloat = 0) {
    translatesAutoresizingMaskIntoConstraints = false
    switch (fromSide, toSide, pinType) {
    case (.left, .right, .equal): leftAnchor.constraint(equalTo: view.rightAnchor, constant: padding).isActive = true
    case (.left, .right, .greaterThanOrEqual): leftAnchor.constraint(greaterThanOrEqualTo: view.rightAnchor, constant: padding).isActive = true
    case (.left, .right, .lessThanOrEqual): leftAnchor.constraint(lessThanOrEqualTo: view.rightAnchor, constant: padding).isActive = true
    case (.right, .left, .equal): rightAnchor.constraint(equalTo: view.leftAnchor, constant: -padding).isActive = true
    case (.right, .left, .greaterThanOrEqual): rightAnchor.constraint(lessThanOrEqualTo: view.leftAnchor, constant: -padding).isActive = true
    case (.right, .left, .lessThanOrEqual): rightAnchor.constraint(greaterThanOrEqualTo: view.leftAnchor, constant: -padding).isActive = true
    case (.left, .left, .equal): leftAnchor.constraint(equalTo: view.leftAnchor, constant: padding).isActive = true
    case (.left, .left, .greaterThanOrEqual): leftAnchor.constraint(greaterThanOrEqualTo: view.leftAnchor, constant: padding).isActive = true
    case (.left, .left, .lessThanOrEqual): leftAnchor.constraint(lessThanOrEqualTo: view.leftAnchor, constant: padding).isActive = true
    case (.right, .right, .equal): rightAnchor.constraint(equalTo: view.rightAnchor, constant: -padding).isActive = true
    case (.right, .right, .greaterThanOrEqual): rightAnchor.constraint(lessThanOrEqualTo: view.rightAnchor, constant: -padding).isActive = true
    case (.right, .right, .lessThanOrEqual): rightAnchor.constraint(greaterThanOrEqualTo: view.rightAnchor, constant: -padding).isActive = true
    }
  }

  // swiftlint:disable cyclomatic_complexity
  func pinVertically(
    from fromSide: VerticalSide,
    to toSide: VerticalSide,
    of view: UIView,
    pinType: PinType = .equal,
    withPadding padding: CGFloat = 0) {
    translatesAutoresizingMaskIntoConstraints = false
    switch (fromSide, toSide, pinType) {
    case (.top, .bottom, .equal): topAnchor.constraint(equalTo: view.bottomAnchor, constant: padding).isActive = true
    case (.top, .bottom, .greaterThanOrEqual): topAnchor.constraint(greaterThanOrEqualTo: view.bottomAnchor, constant: padding).isActive = true
    case (.top, .bottom, .lessThanOrEqual): topAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: padding).isActive = true
    case (.bottom, .top, .equal): bottomAnchor.constraint(equalTo: view.topAnchor, constant: -padding).isActive = true
    case (.bottom, .top, .greaterThanOrEqual): bottomAnchor.constraint(lessThanOrEqualTo: view.topAnchor, constant: -padding).isActive = true
    case (.bottom, .top, .lessThanOrEqual): bottomAnchor.constraint(greaterThanOrEqualTo: view.topAnchor, constant: -padding).isActive = true
    case (.top, .top, .equal): topAnchor.constraint(equalTo: view.topAnchor, constant: padding).isActive = true
    case (.top, .top, .greaterThanOrEqual): topAnchor.constraint(greaterThanOrEqualTo: view.topAnchor, constant: padding).isActive = true
    case (.top, .top, .lessThanOrEqual): topAnchor.constraint(lessThanOrEqualTo: view.topAnchor, constant: padding).isActive = true
    case (.bottom, .bottom, .equal): bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -padding).isActive = true
    case (.bottom, .bottom, .greaterThanOrEqual): bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: -padding).isActive = true
    case (.bottom, .bottom, .lessThanOrEqual): bottomAnchor.constraint(greaterThanOrEqualTo: view.bottomAnchor, constant: -padding).isActive = true
    }
  }
}
