//
//  UITableView+Extensions.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 12.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
  func layoutTableHeaderView() {
    guard let headerView = self.tableHeaderView else { return }
    headerView.translatesAutoresizingMaskIntoConstraints = false

    let headerWidth = headerView.bounds.size.width

    let temporaryWidthConstraints = NSLayoutConstraint.constraints(
      withVisualFormat: "[headerView(width)]",
      options: NSLayoutConstraint.FormatOptions(
        rawValue: UInt(0)),
      metrics: ["width": headerWidth],
      views: ["headerView": headerView])

    headerView.addConstraints(temporaryWidthConstraints)

    headerView.setNeedsLayout()
    headerView.layoutIfNeeded()

    let headerSize = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    let height = headerSize.height
    var frame = headerView.frame

    frame.size.height = height
    headerView.frame = frame

    self.tableHeaderView = headerView

    headerView.removeConstraints(temporaryWidthConstraints)
    headerView.translatesAutoresizingMaskIntoConstraints = true
  }

  func registerCell(_ type: UITableViewCell.Type) {
    let typeName = String(describing: type)
    register(type, forCellReuseIdentifier: typeName)
  }

  func registerHeaderFooter(_ type: UITableViewHeaderFooterView.Type) {
    let typeName = String(describing: type)
    register(type, forHeaderFooterViewReuseIdentifier: typeName)
  }

  func dequeueHeaderFooter<T: UITableViewHeaderFooterView>(_ type: T.Type) -> T? {
    let typeName = String(describing: type)
    return dequeueReusableHeaderFooterView(withIdentifier: typeName) as? T
  }

  func dequeueCell<T: UITableViewCell>(_ type: T.Type) -> T? {
    let typeName = String(describing: type)
    return dequeueReusableCell(withIdentifier: typeName) as? T
  }
}
