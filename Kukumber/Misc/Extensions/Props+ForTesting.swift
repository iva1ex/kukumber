//
//  Props+ForTesting.swift
//  KukumberStorybook
//
//  Created by Alexander Ivashchenko on 28.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

extension MainHomeViewController.Props {
  static let basic = MainHomeViewController.Props(
    title: R.string.localizable.homeTitle(),
    notifications: GeneralNotification.Test.multiple.map { $0.text },
    openProfile: Command { print("Открыть профиль") },
    medicationSection: .init(
      title: R.string.localizable.homeMedicationSectionTitle(),
      allButton: .init(
        title: R.string.localizable.generalAll(),
        action: Command { print("Показать все медикаменты") }),
      medications: Medication.Test.multiple.map { .init(name: $0.name, amountText: $0.amountText, time: $0.time, delete: Command { }, select: Command { }) }),
    vaccinationSection: .init(
      title: R.string.localizable.homeVaccinationSectionTitle(),
      allButton: .init(
        title: R.string.localizable.generalAll(),
        action: Command { print("Показать все медикаменты") }),
      vaccinations: Vaccination.Test.multiple.map { .init(name: $0.name, date: $0.date, delete: Command { }, select: Command { }) })
  )
}

extension NotificationView.Props {
  static let basic = NotificationView.Props(text: GeneralNotification.Test.basic.text)
  static let hugeText = NotificationView.Props(text: GeneralNotification.Test.huge.text)
}

extension TreatmentViewController.Props {
  static let basic = TreatmentViewController.Props(
    currentTreatmentSection: .init(
      title: R.string.localizable.treatmentTreatmentTabCurrentSectionTitle(),
      allButton: .init(
        title: R.string.localizable.generalAll(),
        action: Command { print("Показать все текущее лечение") }),
      treatments: Treatment.Test.multipleCurrent.map { .init(name: $0.name, dateRange: $0.dateRange, delete: Command { }) }),
    archiveTreatmentSection: .init(
      title: R.string.localizable.treatmentTreatmentTabArchiveSectionTitle(),
      allButton: .init(
        title: R.string.localizable.generalAll(),
        action: Command { print("Показать все архивное лечение") }),
      treatments: Treatment.Test.multipleArchived.map { .init(name: $0.name, dateRange: $0.dateRange, delete: Command { }) })
  )
}

extension PreventionViewController.Props {
  static let basic = PreventionViewController.Props()
}

extension ResearchesViewController.Props {
  static let basic = ResearchesViewController.Props(
    title: R.string.localizable.researchesTitle(),
    editButton: .init(
      title: R.string.localizable.generalEdit(),
      action: Command { print("Редактировать архив") }),
    researches: Research.Test.multiple.map { .init(name: $0.name, date: $0.date, delete: Command { }) })
}

extension MedicationDetailsViewController.Props {
  static let basic = MedicationDetailsViewController.Props(title: "Омепразол 20mg")
}

extension VaccinationDetailsViewController.Props {
  static let basic = VaccinationDetailsViewController.Props(title: "Дифтерия, столбняк")
}

extension ProfileViewController.Props {
  static let basic = ProfileViewController.Props(title: "Профиль", back: Command { })
}
