//
//  Props+ForTesting.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 10.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

extension Alert {
  struct Test {
    static let basic: Alert = .init(id: UUID(), text: "Сейчас в Украине грипп! Осторожно!")
    static let huge: Alert = .init(
      id: UUID(),
      text: "Сейчас в Украине грипп! Осторожно! Реально сейчас в Украине грипп! Честно! Не понял?! Мда..")

    static let multiple: [Alert] = [
      .init(
        id: UUID(),
        text: "Сейчас в Украине грипп! Осторожно!"),
      .init(
        id: UUID(),
        text: "Сейчас в Украине грипп! Осторожно! Реально сейчас в Украине грипп! Честно! Не понял?! Мда..")
    ]
  }
}

extension Medication {
  struct Test {
    static let multiple: [Medication] = [
      .init(id: UUID(), name: "Омепразол 20mg", amountText: "1 капсула", time: "03:44"),
      .init(id: UUID(), name: "Дротаверин 40mg", amountText: "1 таблетка", time: "04:18")
    ]
  }
}

extension Vaccination {
  struct Test {
    static let multiple: [Vaccination] = [
    .init(id: UUID(), name: "Дифтерия, столбняк", date: "13 сентября, 2021")
    ]
  }
}

extension Treatment {
  struct Test {
    static let multipleCurrent: [Treatment] = [
      .init(id: UUID(), name: "ВСД", dateRange: "6 марта, 2020 - по настоящее время")
    ]

    static let multipleArchived: [Treatment] = [
      .init(id: UUID(), name: "ОРВИ", dateRange: "11 февраля - 18 февраля, 2020"),
      .init(id: UUID(), name: "Грипп", dateRange: "30 декабря, 2019 - 8 января, 2020"),
      .init(id: UUID(), name: "Перелом ноги", dateRange: "30 декабря, 2019 - 8 января, 2020"),
      .init(id: UUID(), name: "Обострение гастрита", dateRange: "30 декабря, 2019 - 8 января, 2020"),
      .init(id: UUID(), name: "Ониходистрофия", dateRange: "30 декабря, 2019 - 8 января, 2020")
    ]
  }
}

extension Research {
  struct Test {
    static let multiple: [Research] = [
      .init(id: UUID(), name: "Анализ на коронавирус", date: "18 февраля, 2020"),
      .init(id: UUID(), name: "Клинический анализ крови", date: "18 февраля, 2020"),
      .init(id: UUID(), name: "Биохимия крови", date: "18 февраля, 2020"),
      .init(id: UUID(), name: "Тиреоглобулин, антитела (АТТГ)", date: "18 февраля, 2020")
    ]
  }
}
