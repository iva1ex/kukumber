//
//  UIViewController+Extensions.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 22.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func add(childViewController: UIViewController) {
    addChild(childViewController)
    view.addSubview(childViewController.view)
    childViewController.didMove(toParent: self)
  }

  func remove(childViewController: UIViewController) {
    childViewController.willMove(toParent: nil)
    childViewController.view.removeFromSuperview()
    childViewController.removeFromParent()
  }
}
