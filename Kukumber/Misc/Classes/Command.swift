//
//  Command.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 09.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

public final class CommandWith<T> {
  private let action: (T) -> Void
  let file: StaticString
  let line: Int

  init(file: StaticString = #file,
       line: Int = #line,
       _ action: @escaping (T) -> Void) {
    self.file = file
    self.line = line
    self.action = action
  }

  func perform(with value: T) {
    action(value)
  }
}

public final class Command {
  private let action: () -> Void
  let file: StaticString
  let line: Int

  init(file: StaticString = #file,
       line: Int = #line,
       _ action: @escaping () -> Void) {
    self.file = file
    self.line = line
    self.action = action
  }

  func perform() {
    action()
  }
}
