//
//  ObjectDeinitNotifier.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 20.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

var deinitCallbackKey = "DEINITCALLBACK_Kukumber"

class ObjectDeinitNotifier: NSObject {
  static func notify(for object: NSObject, callback: @escaping () -> Void) {
    let rem = deinitCallback(forObject: object)
    rem.callbacks.append(callback)
  }

  static fileprivate func deinitCallback(forObject object: NSObject) -> DeinitCallback {
    if let deinitCallback = objc_getAssociatedObject(object, &deinitCallbackKey) as? DeinitCallback {
      return deinitCallback
    } else {
      let callback = DeinitCallback()
      objc_setAssociatedObject(object, &deinitCallbackKey, callback, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      return callback
    }
  }
}

@objc
private class DeinitCallback: NSObject {
  var callbacks: [() -> Void] = []

  deinit {
    callbacks.forEach({ $0() })
  }
}
