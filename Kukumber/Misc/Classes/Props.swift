//
//  Props.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 10.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

public protocol RenderableProps { }

public protocol PropsRenderable {
  func render(_ props: RenderableProps)
}
