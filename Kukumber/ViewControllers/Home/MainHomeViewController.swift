//
//  MainHomeViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

public extension MainHomeViewController {
  struct Props: RenderableProps {
    let title: String
    let alerts: [AlertView.Props]
    let openProfile: Command
    let medicationSection: MedicationSection; struct MedicationSection {
      let title: String
      let allButton: Button; struct Button {
        let title: String
        let action: Command
      }
      let medications: [MedicationListItem]; struct MedicationListItem {
        let name: String
        let amountText: String
        let time: String
        let delete: Command
        let select: Command
      }
    }

    let vaccinationSection: VaccinationSection; struct VaccinationSection {
      let title: String
      let allButton: Button; struct Button {
        let title: String
        let action: Command
      }
      let vaccinations: [VaccinationListItem]; struct VaccinationListItem {
        let name: String
        let date: String
        let delete: Command
        let select: Command
      }
    }

    static let empty = Props(
      title: "",
      alerts: [],
      openProfile: Command { },
      medicationSection: .init(
        title: "",
        allButton: .init(title: "", action: Command { }),
        medications: []),
      vaccinationSection: .init(
        title: "",
        allButton: .init(title: "", action: Command { }),
        vaccinations: []))
  }
}

public final class MainHomeViewController: BaseViewController {
  private var props = Props.empty

  private lazy var profileButton: UIButton = {
    let button = UIButton(type: .custom)
    button.setImage(R.image.home.profile.nonSelected(), for: .normal)
    button.setImage(R.image.home.profile.selected(), for: .selected)
    button.setImage(R.image.home.profile.selected(), for: .highlighted)

    return button
  }()

  private lazy var tableHeaderView: UIStackView = {
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.spacing = 16
    stackView.layoutMargins = .init(top: 16, left: 16, bottom: 16, right: 16)
    stackView.isLayoutMarginsRelativeArrangement = true

    return stackView
  }()

  private lazy var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .grouped)
    tableView.tableHeaderView = tableHeaderView
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = R.color.background()
    tableView.registerHeaderFooter(LargeTitleTableHeaderView.self)
    tableView.registerCell(MedicationTableViewCell.self)
    tableView.registerCell(VaccinationTableViewCell.self)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44.0
    tableView.separatorStyle = .none
    tableView.allowsSelectionDuringEditing = false

    return tableView
  }()

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }

    configureNavigationBar()

    tableHeaderView.removeAllArrangedSubviews()
    self.props.alerts.forEach {
      let alertView = AlertView()
      alertView.render($0)
      tableHeaderView.addArrangedSubview(alertView)
    }
    tableView.reloadData()
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    profileButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
    configureTableView()
    render(props)
  }

  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tableView.layoutTableHeaderView()
  }

  public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    tableView.layoutTableHeaderView()
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    configureNavigationBar()

    tableView.layoutTableHeaderView()
  }
}

private extension MainHomeViewController {
  func configureTableView() {
    view.addSubview(tableView)
    tableView.pinToSides(.all, to: view)
  }

  @objc
  func barButtonTapped() {
    props.openProfile.perform()
  }

  func configureNavigationBar() {
    navigationController?.navigationBar.topItem?.title = self.props.title
    navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(customView: profileButton)
    navigationController?.navigationBar.prefersLargeTitles = true
  }
}

extension MainHomeViewController: UITableViewDelegate {
  public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    props.selectRow(at: indexPath)
  }
}

extension MainHomeViewController: UITableViewDataSource {
  public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      props.deleteRow(at: indexPath)
    }
  }

  public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    var headerView: LargeTitleTableHeaderView!
    if let unwrappedHeaderView = tableView.dequeueHeaderFooter(LargeTitleTableHeaderView.self) {
      headerView = unwrappedHeaderView
    } else {
      headerView = LargeTitleTableHeaderView(reuseIdentifier: String(describing: LargeTitleTableHeaderView.self))
    }

    let button = props.button(for: section)

    let headerViewProps: LargeTitleTableHeaderView.Props = .init(
      title: props.title(for: section),
      button: .init(title: button.title, action: button.action))

    headerView.render(headerViewProps)
    return headerView
  }

  public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 56
  }

  public func numberOfSections(in tableView: UITableView) -> Int {
    return props.sections.count
  }

  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return props.numberOfRows(in: section)
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let section = props.sections[indexPath.section]
    let cell: UITableViewCell = {
      switch section {
      case .medication:
        guard let medicationCell = tableView.dequeueCell(MedicationTableViewCell.self) else { return UITableViewCell() }
        let medication = props.medicationSection.medications[indexPath.row]
        medicationCell.render(MedicationTableViewCell.Props(name: medication.name, amount: medication.amountText, time: medication.time))
        return medicationCell
      case .vaccination:
        guard let vaccinationCell = tableView.dequeueCell(VaccinationTableViewCell.self) else { return UITableViewCell() }
        let vaccination = props.vaccinationSection.vaccinations[indexPath.row]
        vaccinationCell.render(VaccinationTableViewCell.Props(name: vaccination.name, date: vaccination.date))
        return vaccinationCell
      }
    }()
    return cell
  }
}

extension MainHomeViewController.Props {
  enum SectionType {
    case medication
    case vaccination
  }

  var sections: [SectionType] {
    return [
      (medicationSection.medications.count > 0 ? .medication : nil),
      (vaccinationSection.vaccinations.count > 0 ? .vaccination : nil)
      ].compactMap { $0 }
  }

  func title(for section: Int) -> String {
    switch sections[section] {
    case .medication: return medicationSection.title
    case .vaccination: return vaccinationSection.title
    }
  }

  func numberOfRows(in section: Int) -> Int {
    switch sections[section] {
    case .medication: return medicationSection.medications.count
    case .vaccination: return vaccinationSection.vaccinations.count
    }
  }

  func button(for section: Int) -> (title: String, action: Command) {
    switch sections[section] {
    case .medication: return (title: medicationSection.allButton.title, action: medicationSection.allButton.action)
    case .vaccination: return (title: vaccinationSection.allButton.title, action: vaccinationSection.allButton.action)
    }
  }

  func deleteRow(at indexPath: IndexPath) {
    switch sections[indexPath.section] {
    case .medication: medicationSection.medications[indexPath.row].delete.perform()
    case .vaccination: vaccinationSection.vaccinations[indexPath.row].delete.perform()
    }
  }

  func selectRow(at indexPath: IndexPath) {
    switch sections[indexPath.section] {
    case .medication: medicationSection.medications[indexPath.row].select.perform()
    case .vaccination: vaccinationSection.vaccinations[indexPath.row].select.perform()
    }
  }
}
