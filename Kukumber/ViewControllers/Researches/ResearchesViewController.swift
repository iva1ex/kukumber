//
//  ResearchesViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

public extension ResearchesViewController {
  struct Props: RenderableProps {
    let title: String
    let editButton: Button; struct Button {
      let title: String
      let action: Command
    }

    let researches: [ResearchListItem]; struct ResearchListItem {
      let name: String
      let date: String
      let delete: Command
    }
    static let empty = Props(
      title: "",
      editButton: .init(title: "", action: Command { }),
      researches: [])
  }
}

public final class ResearchesViewController: BaseViewController {
  private var props = Props.empty

  private lazy var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .grouped)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = R.color.background()
    tableView.registerCell(ResearchTableViewCell.self)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44.0
    tableView.separatorStyle = .none
    tableView.allowsSelectionDuringEditing = false

    return tableView
  }()

  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = R.color.background()
    configureTableView()
    configureNavigationBar()
    render(props)
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    configureNavigationBar()
  }

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }

    configureNavigationBar()
    tableView.reloadData()
  }
}

private extension ResearchesViewController {
  func configureTableView() {
    view.addSubview(tableView)
    tableView.pinToSides(.all, to: view)
  }

  func configureNavigationBar() {
    navigationController?.navigationBar.topItem?.title = self.props.title
    navigationController?.navigationBar.topItem?.rightBarButtonItem = .init(
      title: self.props.editButton.title,
      style: .plain,
      target: self,
      action: #selector(barButtonTapped))
    navigationController?.navigationBar.prefersLargeTitles = true
  }

  @objc
  func barButtonTapped() {
    props.editButton.action.perform()
  }
}

extension ResearchesViewController: UITableViewDelegate {

}

extension ResearchesViewController: UITableViewDataSource {
  public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      props.researches[indexPath.row].delete.perform()
    }
  }

  public func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return props.researches.count
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let researchCell = tableView.dequeueCell(ResearchTableViewCell.self) else { return UITableViewCell() }
    let research = props.researches[indexPath.row]
    let cellProps: ResearchTableViewCell.Props = .init(
      name: research.name,
      date: research.date)
    researchCell.render(cellProps)

    return researchCell
  }
}
