//
//  SettingsViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit
import Rswift

public extension SettingsViewController {
  struct Props: RenderableProps {
    let title: String

    let sections: [Section]; struct Section {
      let rows: [SettingsCell.Props]
    }

    static let empty = Props(
      title: "",
      sections: [])
  }
}

public final class SettingsViewController: BaseViewController {
  private var props = Props.empty

  private lazy var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .grouped)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = R.color.background()
     tableView.registerCell(SettingsCell.self)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44.0
    tableView.separatorStyle = .none
    tableView.allowsSelectionDuringEditing = false
    tableView.contentInset = .init(top: 16, left: 0, bottom: 0, right: 0)
    return tableView
  }()

  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = R.color.background()
    configureTableView()
    configureNavigationBar()
  }

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }
    configureNavigationBar()
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    configureNavigationBar()
  }
}

private extension SettingsViewController {
  func configureTableView() {
    view.addSubview(tableView)
    tableView.pinToSides(.all, to: view)
  }

  func configureNavigationBar() {
    navigationController?.navigationBar.prefersLargeTitles = true
    navigationController?.navigationBar.topItem?.title = props.title
  }
}

extension SettingsViewController: UITableViewDelegate { }

extension SettingsViewController: UITableViewDataSource {
  public func numberOfSections(in tableView: UITableView) -> Int {
    return props.sections.count
  }

  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return props.sections[section].rows.count
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueCell(SettingsCell.self) else {
      return UITableViewCell()
    }
    let row = props.sections[indexPath.section].rows[indexPath.row]
    cell.render(row)
    return cell
  }
}
