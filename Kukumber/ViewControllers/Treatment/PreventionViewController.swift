//
//  PreventionViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 20.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

public extension PreventionViewController {
  struct Props: RenderableProps {
    static let empty = Props()
  }
}

public final class PreventionViewController: BaseViewController {
  private var props = Props.empty

  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = R.color.background()
  }

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }
  }
}

private extension PreventionViewController {
}
