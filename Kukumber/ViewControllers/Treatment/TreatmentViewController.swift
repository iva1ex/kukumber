//
//  TreatmentViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

public extension TreatmentViewController {
  struct Props: RenderableProps {
    struct TreatmentSection {
      let title: String
      let allButton: Button; struct Button {
        let title: String
        let action: Command
      }
      let treatments: [Treatment]; struct Treatment {
        let name: String
        let dateRange: String
        let delete: Command
      }
    }
    var currentTreatmentSection: TreatmentSection
    var archiveTreatmentSection: TreatmentSection

    static let empty = Props(
      currentTreatmentSection: .init(
        title: "",
        allButton: .init(title: "", action: Command { }),
        treatments: []),
      archiveTreatmentSection: .init(
        title: "",
        allButton: .init(title: "", action: Command { }),
        treatments: []))
  }
}

public final class TreatmentViewController: BaseViewController {
  private var props = Props.empty

  private lazy var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .grouped)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = R.color.background()
    tableView.registerHeaderFooter(LargeTitleTableHeaderView.self)
    tableView.registerCell(TreatmentTableViewCell.self)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44.0
    tableView.separatorStyle = .none
    tableView.allowsSelectionDuringEditing = false

    return tableView
  }()

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = R.color.background()
    configureTableView()
    render(props)
  }

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }

    tableView.reloadData()
  }
}

private extension TreatmentViewController {
  func configureTableView() {
    view.addSubview(tableView)
    tableView.pinToSides(.all, to: view)
  }
}

extension TreatmentViewController: UITableViewDelegate {

}

extension TreatmentViewController: UITableViewDataSource {
  public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
        switch props.sections[indexPath.section] {
        case .current: return props.currentTreatmentSection.treatments[indexPath.row].delete.perform()
        case .archive: return props.archiveTreatmentSection.treatments[indexPath.row].delete.perform()
        }
    }
  }

  public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    var headerView: LargeTitleTableHeaderView!
    if let unwrappedHeaderView = tableView.dequeueHeaderFooter(LargeTitleTableHeaderView.self) {
      headerView = unwrappedHeaderView
    } else {
      headerView = LargeTitleTableHeaderView(reuseIdentifier: String(describing: LargeTitleTableHeaderView.self))
    }

    let button = props.button(for: section)

    let headerViewProps: LargeTitleTableHeaderView.Props = .init(
      title: props.title(for: section),
      button: .init(title: button.title, action: button.action))

    headerView.render(headerViewProps)
    return headerView
  }

  public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 56
  }

  public func numberOfSections(in tableView: UITableView) -> Int {
    return props.sections.count
  }

  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return props.numberOfRows(in: section)
  }

  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let treatmentCell = tableView.dequeueCell(TreatmentTableViewCell.self) else { return UITableViewCell() }
    let section = props.sections[indexPath.section]
    let treatment: Props.TreatmentSection.Treatment = {
      switch section {
      case .current: return props.currentTreatmentSection.treatments[indexPath.row]
      case .archive: return props.archiveTreatmentSection.treatments[indexPath.row]
      }
    }()
    let cellProps: TreatmentTableViewCell.Props = .init(name: treatment.name, dateRange: treatment.dateRange)
    treatmentCell.render(cellProps)
    return treatmentCell
  }
}

extension TreatmentViewController.Props {
  enum SectionType {
    case current
    case archive
  }

  var sections: [SectionType] {
    return [
      (currentTreatmentSection.treatments.count > 0 ? .current : nil),
      (archiveTreatmentSection.treatments.count > 0 ? .archive : nil)
      ].compactMap { $0 }
  }

  func title(for section: Int) -> String {
    switch sections[section] {
    case .current: return currentTreatmentSection.title
    case .archive: return archiveTreatmentSection.title
    }
  }

  func numberOfRows(in section: Int) -> Int {
    switch sections[section] {
    case .current: return currentTreatmentSection.treatments.count
    case .archive: return archiveTreatmentSection.treatments.count
    }
  }

  func button(for section: Int) -> (title: String, action: Command) {
    switch sections[section] {
    case .current: return (title: currentTreatmentSection.allButton.title, action: currentTreatmentSection.allButton.action)
    case .archive: return (title: archiveTreatmentSection.allButton.title, action: archiveTreatmentSection.allButton.action)
    }
  }
}
