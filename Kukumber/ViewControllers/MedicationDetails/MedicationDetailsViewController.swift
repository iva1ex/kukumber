//
//  MedicationDetailsViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

public extension MedicationDetailsViewController {
  struct Props: RenderableProps {
    let title: String

    static let empty = Props(title: "")
  }
}

public final class MedicationDetailsViewController: BaseViewController {
  private var props = Props.empty

  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = R.color.background()
    configureNavigationBar()
  }

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }
    configureNavigationBar()
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    configureNavigationBar()
  }
}

private extension MedicationDetailsViewController {
  func configureNavigationBar() {
    navigationController?.navigationBar.topItem?.largeTitleDisplayMode = .never
    navigationController?.navigationBar.topItem?.title = props.title
  }
}
