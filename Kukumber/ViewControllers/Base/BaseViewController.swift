//
//  BaseViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

public class BaseViewController: UIViewController, PropsRenderable {
  deinit {
    print("\(type(of: self)) is destroyed")
  }

  public required init() {
    super.init(nibName: nil, bundle: nil)
  }

  public required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  public func render(_ props: RenderableProps) {
    fatalError("render(props:) has not been implemented")
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = R.color.background()
  }
}
