//
//  ViewControllerFactory.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol ViewControllerFactoryProtocol: NSObject {
  func makeMainHomeViewController() -> MainHomeViewController
  func makeTreatmentViewController() -> TreatmentViewController
  func makePreventionViewController() -> PreventionViewController
  func makeAddStubViewController() -> AddStubViewController
  func makeResearchesViewController() -> ResearchesViewController
  func makeSettingsViewController() -> SettingsViewController
  func makeMedicationDetailsViewController() -> MedicationDetailsViewController
  func makeVaccinationDetailsViewController() -> VaccinationDetailsViewController
  func makeProfileViewController() -> ProfileViewController
  func makeAlertDetailsViewController() -> AlertDetailsViewController
}

final class ViewControllerFactory: NSObject, ViewControllerFactoryProtocol {
  func makeMainHomeViewController() -> MainHomeViewController {
    return MainHomeViewController.init()
  }

  func makeTreatmentViewController() -> TreatmentViewController {
    return TreatmentViewController.init()
  }

  func makePreventionViewController() -> PreventionViewController {
    return PreventionViewController.init()
  }

  func makeAddStubViewController() -> AddStubViewController {
    return AddStubViewController.init()
  }

  func makeResearchesViewController() -> ResearchesViewController {
    return ResearchesViewController.init()
  }

  func makeSettingsViewController() -> SettingsViewController {
    return SettingsViewController.init()
  }

  func makeMedicationDetailsViewController() -> MedicationDetailsViewController {
    return MedicationDetailsViewController.init()
  }

  func makeVaccinationDetailsViewController() -> VaccinationDetailsViewController {
    return VaccinationDetailsViewController.init()
  }

  func makeProfileViewController() -> ProfileViewController {
    return ProfileViewController.init()
  }

  func makeAlertDetailsViewController() -> AlertDetailsViewController {
    return AlertDetailsViewController.init()
  }
}
