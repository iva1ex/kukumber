//
//  ProfileViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 03.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

public extension ProfileViewController {
  struct Props: RenderableProps {
    let title: String
    let back: Command

    static let empty = Props(title: "", back: Command { })
  }
}

public final class ProfileViewController: BaseViewController {
  private var props = Props.empty

  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = R.color.background()
    configureNavigationBar()
  }

  public override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    guard isViewLoaded else { return }
    configureNavigationBar()
  }

  public override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    configureNavigationBar()
  }
}

private extension ProfileViewController {
  func configureNavigationBar() {
    navigationController?.navigationBar.topItem?.largeTitleDisplayMode = .never
    navigationController?.navigationBar.topItem?.rightBarButtonItem = .init(barButtonSystemItem: .done, target: self, action: #selector(goBack))
    navigationController?.navigationBar.topItem?.title = props.title
  }

  @objc
  func goBack() {
    props.back.perform()
  }
}
