//
//  AddStubViewController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 08.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

public final class AddStubViewController: BaseViewController {
  public required init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  public required init() {
    super.init()
    tabBarItem = UITabBarItem(
      title: R.string.localizable.tabBarAdd(),
      image: R.image.tabBar.add.nonSelected(),
      selectedImage: R.image.tabBar.add.selected())
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  public required init(props: PropsRenderable) {
    fatalError("init(props:) has not been implemented")
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

  }
}
