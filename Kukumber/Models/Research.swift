//
//  Research.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 28.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

struct Research {
  let id: UUID
  let name: String
  let date: String
}
