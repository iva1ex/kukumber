//
//  Vaccination.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 27.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

struct Vaccination {
  let id: UUID
  let name: String
  let date: String
}
