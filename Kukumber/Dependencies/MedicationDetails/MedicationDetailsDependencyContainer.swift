//
//  MedicationDetailsDependencyContainer.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol MedicationDetailsDependencyContainer: ViewControllerFactoryContainer,
DataSourceFactoryContainer {
  var viewControllerFactory: ViewControllerFactoryProtocol { get }
  var dataSourceFactory: DataSourceFactoryProtocol { get }
}
