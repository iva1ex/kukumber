//
//  DependencyContainer.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol ViewControllerFactoryContainer {
  var viewControllerFactory: ViewControllerFactoryProtocol { get }
}

protocol FlowControllerFactoryContainer {
  var flowControllerFactory: FlowControllerFactoryProtocol { get }
}

protocol DataSourceFactoryContainer {
  var dataSourceFactory: DataSourceFactoryProtocol { get }
}

struct DependencyContainer: ViewControllerFactoryContainer,
  FlowControllerFactoryContainer,
DataSourceFactoryContainer {
  let viewControllerFactory: ViewControllerFactoryProtocol
  let flowControllerFactory: FlowControllerFactoryProtocol
  let dataSourceFactory: DataSourceFactoryProtocol

  static func make() -> DependencyContainer {
    return .init(
      viewControllerFactory: ViewControllerFactory(),
      flowControllerFactory: FlowControllerFactory(),
      dataSourceFactory: DataSourceFactory())
  }
}

extension DependencyContainer: MainTabBarDependencyContainer { }
