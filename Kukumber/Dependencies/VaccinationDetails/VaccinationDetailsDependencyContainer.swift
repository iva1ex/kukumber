//
//  VaccinationDetailsDependencyContainer.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol VaccinationDetailsDependencyContainer: ViewControllerFactoryContainer,
DataSourceFactoryContainer {
  var viewControllerFactory: ViewControllerFactoryProtocol { get }
  var dataSourceFactory: DataSourceFactoryProtocol { get }
}
