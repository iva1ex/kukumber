//
//  HomeDependencyContainer.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol HomeDependencyContainer:
MedicationDetailsDependencyContainer, VaccinationDetailsDependencyContainer, ProfileDependencyContainer, AlertDependencyContainer {
  var viewControllerFactory: ViewControllerFactoryProtocol { get }
  var flowControllerFactory: FlowControllerFactoryProtocol { get }
  var dataSourceFactory: DataSourceFactoryProtocol { get }
}
