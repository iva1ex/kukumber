//
//  ResearchesDependencyContainer.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol ResearchesDependencyContainer:
ViewControllerFactoryContainer,
FlowControllerFactoryContainer,
DataSourceFactoryContainer {
  var viewControllerFactory: ViewControllerFactoryProtocol { get }
  var flowControllerFactory: FlowControllerFactoryProtocol { get }
  var dataSourceFactory: DataSourceFactoryProtocol { get }
}
