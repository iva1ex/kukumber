//
//  AlertDependencyContainer.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 17.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol AlertDependencyContainer: ViewControllerFactoryContainer,
DataSourceFactoryContainer {
  var viewControllerFactory: ViewControllerFactoryProtocol { get }
  var dataSourceFactory: DataSourceFactoryProtocol { get }
}
