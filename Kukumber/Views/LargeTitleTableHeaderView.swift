//
//  LargeTitleTableHeaderView.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 14.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

extension LargeTitleTableHeaderView {
  struct Props: RenderableProps {
    let title: String
    let button: Button; struct Button {
      let title: String
      let action: Command
    }

    static let empty = Props(
      title: "",
      button: .init(title: "", action: Command { }))
  }
}

final class LargeTitleTableHeaderView: UITableViewHeaderFooterView, PropsRenderable {
  private var props = Props.empty

  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    setupView()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = R.color.text.primary()
    label.font = .systemFont(ofSize: 28, weight: .bold)
    return label
  }()

  private lazy var actionButton: UIButton = {
    let button = UIButton(type: .custom)
    button.setTitleColor(R.color.tint(), for: .normal)
    return button
  }()

  func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    titleLabel.text = self.props.title
    actionButton.setTitle(self.props.button.title, for: .normal)
  }
}

private extension LargeTitleTableHeaderView {
  func setupView() {
    setupSubviews()
    setupAppearance()
    setupLayout()
  }

  func setupSubviews() {
    addSubview(titleLabel)
    addSubview(actionButton)
  }

  func setupAppearance() {
    actionButton.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
  }

  func setupLayout() {
    titleLabel.pinToSides([.top, .bottom], to: self)
    titleLabel.pinToSides([.left], to: self, withPadding: 16)
    actionButton.pinToSides([.top, .bottom], to: self)
    actionButton.pinToSides([.right], to: self, withPadding: 16)
    titleLabel.pinHorizontally(from: .right, to: .left, of: actionButton, pinType: .greaterThanOrEqual, withPadding: 16)
  }

  @objc
  func actionButtonTapped() {
    props.button.action.perform()
  }

}
