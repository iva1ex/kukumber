//
//  BaseView.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 10.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

class BaseView: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func render(_ props: RenderableProps) {
    fatalError("render(props:) has not been implemented")
  }

  func setupView() {
    setupSubviews()
    setupAppearance()
    setupLayout()
  }

  override func layoutSubviews() {
    super.layoutSubviews()

    subviews.forEach { subview in
      guard let label = subview as? UILabel, label.numberOfLines == 0 else { return  }
      label.preferredMaxLayoutWidth = label.frame.width
    }
  }

  func setupSubviews() {
    fatalError("setupSubviews() has not been implemented")
  }

  func setupAppearance() {
    fatalError("setupAppearance() has not been implemented")
  }

  func setupLayout() {
    fatalError("setupLayout() has not been implemented")
  }
}
