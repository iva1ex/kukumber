//
//  AlertView.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 10.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

extension AlertView {
  struct Props: RenderableProps {
    let text: String
    let select: Command

    static let empty = Props(text: "", select: Command { })
  }
}

final class AlertView: BaseView, PropsRenderable {
  private var props = Props.empty

  private lazy var textLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = R.color.text.tinted()
    label.font = .systemFont(ofSize: 17, weight: .regular)

    return label
  }()

  private lazy var tapGestureRecognizer: UITapGestureRecognizer = {
    let tapGestureRecognizer = UITapGestureRecognizer()
    tapGestureRecognizer.numberOfTapsRequired = 1
    tapGestureRecognizer.numberOfTouchesRequired = 1
    return tapGestureRecognizer
  }()

  override func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    textLabel.text = self.props.text
  }

  override func setupSubviews() {
    addSubview(textLabel)
  }

  override func setupAppearance() {
    backgroundColor = R.color.tint()
    layer.cornerRadius = 6
    clipsToBounds = true
    tapGestureRecognizer.addTarget(self, action: #selector(tapped))
    addGestureRecognizer(tapGestureRecognizer)
  }

  override func setupLayout() {
    textLabel.pinToSides(.all, to: self, withPadding: 16)
  }
}

private extension AlertView {
  @objc
  func tapped() {
    props.select.perform()
  }
}
