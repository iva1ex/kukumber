//
//  SettingsCell.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 17.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

extension SettingsCell {
  struct Props: RenderableProps {
    let image: UIImage?
    let title: String
    let content: AccessoryContent; enum AccessoryContent {
      case detailedText(text: String)
      case switchItem(isOn: Bool, update: CommandWith<Bool>)
      case none
    }

    static let empty = Props.init(image: nil, title: "", content: .none)
  }
}

final class SettingsCell: UITableViewCell, PropsRenderable {
  private var props = Props.empty

  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.textColor = R.color.text.primary()
    label.font = .systemFont(ofSize: 17, weight: .regular)
    return label
  }()

  private lazy var switchControl: UISwitch = {
    let switchControl = UISwitch()
    return switchControl
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    setupView()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    imageView?.image = self.props.image
    textLabel?.text = self.props.title
    configure(self.props.content)
  }
}

private extension SettingsCell {
  func configure(_ content: Props.AccessoryContent) {
    switch content {
    case .detailedText(let text):
      detailTextLabel?.text = text
    case .switchItem(let isOn, _):
      switchControl.isOn = isOn
      switchControl.addTarget(self, action: #selector(switchControlUpdated(_:)), for: .valueChanged)
      accessoryView = switchControl
    case .none:
      return
    }
  }

  @objc
  func switchControlUpdated(_ switchControl: UISwitch) {
    props.switchUpdate?.perform(with: switchControl.isOn)
  }

  func setupView() {
    setupAppearance()
  }

  func setupAppearance() {
    backgroundColor = R.color.cardBackground()
    selectionStyle = .none
  }
}

extension SettingsCell.Props {
  var switchUpdate: CommandWith<Bool>? {
    guard case .switchItem(_, let update) = content else { return nil }
    return update
  }
}
