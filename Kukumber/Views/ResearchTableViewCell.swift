//
//  ResearchTableViewCell.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 22.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

extension ResearchTableViewCell {
  struct Props: RenderableProps {
    let name: String
    let date: String

    static let empty = Props.init(name: "", date: "")
  }
}

final class ResearchTableViewCell: UITableViewCell, PropsRenderable {
  private var props = Props.empty

  private lazy var nameLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = R.color.text.primary()
    label.font = .systemFont(ofSize: 17, weight: .regular)
    return label
  }()

  private lazy var dateLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = R.color.text.secondary()
    label.font = .systemFont(ofSize: 13, weight: .regular)
    return label
  }()

  private lazy var containerView: UIView = {
    let view = UIView()
    view.backgroundColor = R.color.cardBackground()
    view.layer.cornerRadius = 6
    view.clipsToBounds = true
    return view
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func render(_ props: RenderableProps) {
    self.props = (props as? Props) ?? .empty
    nameLabel.text = self.props.name
    dateLabel.text = self.props.date
  }
}

private extension ResearchTableViewCell {
  func setupView() {
    setupSubviews()
    setupAppearance()
    setupLayout()
  }

  func setupSubviews() {
    addSubview(containerView)
    containerView.addSubview(nameLabel)
    containerView.addSubview(dateLabel)
  }

  func setupAppearance() {
    backgroundColor = R.color.background()
    selectionStyle = .none
  }

  func setupLayout() {
    containerView.pinToSides([.left, .right], to: self, withPadding: 16)
    containerView.pinToSides([.top, .bottom], to: self, withPadding: 4)

    nameLabel.pinToSides([.top, .left, .right], to: containerView, withPadding: 16)
    dateLabel.pinToSides([.left, .bottom, .right], to: containerView, withPadding: 16)
    dateLabel.pinVertically(from: .top, to: .bottom, of: nameLabel, withPadding: 3)
  }
}
