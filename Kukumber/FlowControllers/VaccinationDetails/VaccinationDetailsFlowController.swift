//
//  VaccinationDetailsFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class VaccinationDetailsFlowController: BaseFlowController {
  let dependencyContainer: VaccinationDetailsDependencyContainer
  private let viewController: VaccinationDetailsViewController
  private let dataSource: VaccinationDetailsDataSource

  private let embeddedNavigationController: UINavigationController

  required init(
    dependencyContainer: VaccinationDetailsDependencyContainer,
    navigationController: UINavigationController,
    vaccination: Vaccination) {
    self.dependencyContainer = dependencyContainer
    self.embeddedNavigationController = navigationController

    self.viewController = dependencyContainer
      .viewControllerFactory
      .makeVaccinationDetailsViewController()

    self.dataSource = dependencyContainer
      .dataSourceFactory
      .makeVaccinationDetailsDataSource(viewController: viewController, vaccination: vaccination)

    super.init(nibName: nil, bundle: nil)
    navigationItem.largeTitleDisplayMode = .never
    navigationItem.title = vaccination.name

    add(childViewController: viewController)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    dataSource.render()
  }
}
