//
//  ProfileFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 03.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class ProfileFlowController: BaseFlowController {
  let dependencyContainer: ProfileDependencyContainer
  private let viewController: ProfileViewController
  private let dataSource: ProfileDataSource

  private let embeddedNavigationController: UINavigationController

  required init(dependencyContainer: ProfileDependencyContainer) {
    self.dependencyContainer = dependencyContainer
    self.embeddedNavigationController = UINavigationController()

    self.viewController = dependencyContainer
      .viewControllerFactory
      .makeProfileViewController()

    self.dataSource = dependencyContainer
      .dataSourceFactory
      .makeProfileDataSource(viewController: viewController)

    super.init(nibName: nil, bundle: nil)
    navigationItem.largeTitleDisplayMode = .never
    navigationItem.title = R.string.localizable.profileTitle()

    add(childViewController: embeddedNavigationController)
    dataSource.delegate = self
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    dataSource.render()
    embeddedNavigationController.viewControllers = [viewController]
  }
}

extension ProfileFlowController: ProfileDataSourceDelegate {
  func didDataSourceViewControllerDestroy(_ dataSource: ProfileDataSource) {
  }

  func didGoBack(_ dataSource: ProfileDataSource) {
    dismiss(animated: true)
  }
}
