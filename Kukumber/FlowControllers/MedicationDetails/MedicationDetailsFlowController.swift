//
//  MedicationDetailsFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class MedicationDetailsFlowController: BaseFlowController {
  let dependencyContainer: MedicationDetailsDependencyContainer
  private let viewController: MedicationDetailsViewController
  private let dataSource: MedicationDetailsDataSource

  private let embeddedNavigationController: UINavigationController

  required init(
    dependencyContainer: MedicationDetailsDependencyContainer,
    navigationController: UINavigationController,
    medication: Medication) {
    self.dependencyContainer = dependencyContainer
    self.embeddedNavigationController = navigationController

    self.viewController = dependencyContainer
      .viewControllerFactory
      .makeMedicationDetailsViewController()

    self.dataSource = dependencyContainer
      .dataSourceFactory
      .makeMedicationDetailsDataSource(viewController: viewController, medication: medication)

    super.init(nibName: nil, bundle: nil)
    navigationItem.largeTitleDisplayMode = .never
    navigationItem.title = medication.name

    add(childViewController: viewController)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    dataSource.render()
  }
}
