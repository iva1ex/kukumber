//
//  BaseFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

class BaseFlowController: UIViewController {
  deinit {
    print("\(type(of: self)) is destroyed")
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    children.first?.view.frame = view.bounds
  }
}
