//
//  FlowControllerFactory.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

protocol FlowControllerFactoryProtocol: NSObject {
  func makeMainTabBarFlowController(dependencyContainer: MainTabBarDependencyContainer) -> MainTabBarFlowController
  func makeHomeFlowController(dependencyContainer: HomeDependencyContainer) -> HomeFlowController
  func makeTreatmentFlowController(dependencyContainer: TreatmentDependencyContainer) -> TreatmentFlowController
  func makeResearchesFlowController(dependencyContainer: ResearchesDependencyContainer) -> ResearchesFlowController
  func makeSettingsFlowController(dependencyContainer: SettingsDependencyContainer) -> SettingsFlowController

  func makeMedicationDetailsFlowController(
    dependencyContainer: MedicationDetailsDependencyContainer,
    navigationController: UINavigationController,
    medication: Medication)
    -> MedicationDetailsFlowController

  func makeVaccinationDetailsFlowController(
  dependencyContainer: VaccinationDetailsDependencyContainer,
  navigationController: UINavigationController,
  vaccination: Vaccination)
    -> VaccinationDetailsFlowController

  func makeAlertFlowController(
    dependencyContainer: AlertDependencyContainer,
    alert: Alert)
    -> AlertDetailsFlowController

  func makeProfileFlowController(dependencyContainer: ProfileDependencyContainer) -> ProfileFlowController
}

final class FlowControllerFactory: NSObject, FlowControllerFactoryProtocol {
  func makeMainTabBarFlowController(dependencyContainer: MainTabBarDependencyContainer) -> MainTabBarFlowController {
    return MainTabBarFlowController(dependencyContainer: dependencyContainer)
  }

  func makeHomeFlowController(dependencyContainer: HomeDependencyContainer) -> HomeFlowController {
    return HomeFlowController(dependencyContainer: dependencyContainer)
  }

  func makeTreatmentFlowController(dependencyContainer: TreatmentDependencyContainer) -> TreatmentFlowController {
    return TreatmentFlowController(dependencyContainer: dependencyContainer)
  }

  func makeResearchesFlowController(dependencyContainer: ResearchesDependencyContainer) -> ResearchesFlowController {
    return ResearchesFlowController(dependencyContainer: dependencyContainer)
  }

  func makeSettingsFlowController(dependencyContainer: SettingsDependencyContainer) -> SettingsFlowController {
    return SettingsFlowController(dependencyContainer: dependencyContainer)
  }

  func makeMedicationDetailsFlowController(
  dependencyContainer: MedicationDetailsDependencyContainer,
  navigationController: UINavigationController,
  medication: Medication)
    -> MedicationDetailsFlowController {
    return MedicationDetailsFlowController(dependencyContainer: dependencyContainer, navigationController: navigationController, medication: medication)
  }

  func makeVaccinationDetailsFlowController(
  dependencyContainer: VaccinationDetailsDependencyContainer,
  navigationController: UINavigationController,
  vaccination: Vaccination)
    -> VaccinationDetailsFlowController {
    return VaccinationDetailsFlowController(dependencyContainer: dependencyContainer, navigationController: navigationController, vaccination: vaccination)
  }

  func makeAlertFlowController(
    dependencyContainer: AlertDependencyContainer,
    alert: Alert)
    -> AlertDetailsFlowController {
      return AlertDetailsFlowController(dependencyContainer: dependencyContainer, alert: alert)
  }

  func makeProfileFlowController(dependencyContainer: ProfileDependencyContainer) -> ProfileFlowController {
  return ProfileFlowController(dependencyContainer: dependencyContainer)
  }

}
