//
//  AppFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 22.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class AppFlowController: BaseFlowController {
  let dependencyContainer: DependencyContainer

  func start() {
    startMain()
  }

  required init(dependencyContainer: DependencyContainer) {
    self.dependencyContainer = dependencyContainer
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func startMain() {
    let mainTabBarFlowController = dependencyContainer
      .flowControllerFactory
      .makeMainTabBarFlowController(dependencyContainer: dependencyContainer)
    mainTabBarFlowController.delegate = self
    add(childViewController: mainTabBarFlowController)
    mainTabBarFlowController.start()
  }
}

// MARK: – MainTabBarFlowControllerDelegate conformance
extension AppFlowController: MainTabBarFlowControllerDelegate {
}
