//
//  SettingsFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class SettingsFlowController: BaseFlowController {
  let dependencyContainer: SettingsDependencyContainer
  private var embeddedNavigationController: UINavigationController!
  private let dataSource: SettingsDataSource
  private let viewController: SettingsViewController

  required init(dependencyContainer: SettingsDependencyContainer) {
    self.dependencyContainer = dependencyContainer

    viewController = dependencyContainer
    .viewControllerFactory
    .makeSettingsViewController()

    dataSource = dependencyContainer
      .dataSourceFactory
      .makeSettingsDataSource(viewController: viewController)

    super.init(nibName: nil, bundle: nil)

    embeddedNavigationController = UINavigationController()
    embeddedNavigationController.navigationBar.prefersLargeTitles = true
    tabBarItem = UITabBarItem(
      title: R.string.localizable.tabBarSettings(),
      image: R.image.tabBar.settings.nonSelected(),
      selectedImage: R.image.tabBar.settings.selected())
    add(childViewController: embeddedNavigationController)
  }

  convenience required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    embeddedNavigationController.viewControllers = [viewController]
    dataSource.render()
  }
}
