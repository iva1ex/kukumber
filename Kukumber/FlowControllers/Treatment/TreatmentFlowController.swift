//
//  TreatmentFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

enum TreatmentScreenType: Int {
  case treatment
  case prevention
}

final class TreatmentFlowController: BaseFlowController {
  let dependencyContainer: TreatmentDependencyContainer
  private var embeddedNavigationController: UINavigationController!
  private var containerViewController: UIViewController!

  private let treatmentViewController: TreatmentViewController
  private let preventionViewController: PreventionViewController

  private let treatmentDataSource: TreatmentDataSource
  private let preventionDataSource: PreventionDataSource

  required init(dependencyContainer: TreatmentDependencyContainer) {
    self.dependencyContainer = dependencyContainer
    treatmentViewController = dependencyContainer
      .viewControllerFactory
      .makeTreatmentViewController()

    preventionViewController = dependencyContainer
      .viewControllerFactory
      .makePreventionViewController()

    treatmentDataSource = dependencyContainer
      .dataSourceFactory
      .makeTreatmentDataSource(viewController: treatmentViewController)

    preventionDataSource = dependencyContainer
      .dataSourceFactory
      .makePreventionDataSource(viewController: preventionViewController)

    super.init(nibName: nil, bundle: nil)

    embeddedNavigationController = UINavigationController()
    containerViewController = UIViewController()
    tabBarItem = UITabBarItem(
      title: R.string.localizable.tabBarTreatment(),
      image: R.image.tabBar.treatment.nonSelected(),
      selectedImage: R.image.tabBar.treatment.selected())
    add(childViewController: embeddedNavigationController)
    addSegmentedControl()
  }

  convenience required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    clearNavigationBarBottomShadow()

    treatmentDataSource.render()
    preventionDataSource.render()

    embeddedNavigationController.viewControllers = [containerViewController]
    present(.treatment)
  }
}

private extension TreatmentFlowController {
  func clearNavigationBarBottomShadow() {
//    if #available(iOS 13.0, *) {
//      let navigationBarAppearence = UINavigationBarAppearance()
//      navigationBarAppearence.shadowColor = .clear
//      embeddedNavigationController.navigationBar.scrollEdgeAppearance = navigationBarAppearence
//      embeddedNavigationController.navigationBar.clipsToBounds = true
//    } else { }
  }

  func addSegmentedControl() {
    let titles = [
      R.string.localizable.treatmentTreatmentTabTitle(),
      R.string.localizable.treatmentPreventionTabTitle()
  ]
    let segmentedControl = UISegmentedControl(items: titles)
    segmentedControl.selectedSegmentIndex = 0
    segmentedControl.bounds = .init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: segmentedControl.bounds.height)
    segmentedControl.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
    containerViewController.navigationItem.titleView = segmentedControl
  }

  @objc
  func segmentedControlChanged(_ sender: UISegmentedControl) {
    guard let screenTypeToPresent = TreatmentScreenType(rawValue: sender.selectedSegmentIndex) else {
      return
    }
    present(screenTypeToPresent)
  }

  func present(_ screenType: TreatmentScreenType) {
    switch screenType {
    case .treatment:
      containerViewController.remove(childViewController: preventionViewController)
      containerViewController.add(childViewController: treatmentViewController)
    case .prevention:
      containerViewController.remove(childViewController: treatmentViewController)
      containerViewController.add(childViewController: preventionViewController)
    }
  }
}
