//
//  ResearchesFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class ResearchesFlowController: BaseFlowController {
  let dependencyContainer: ResearchesDependencyContainer
  private var embeddedNavigationController: UINavigationController!

  private let researchesViewController: ResearchesViewController
  private let researchesDataSource: ResearchesDataSource

  required init(dependencyContainer: ResearchesDependencyContainer) {
    self.dependencyContainer = dependencyContainer

    researchesViewController = dependencyContainer
      .viewControllerFactory
      .makeResearchesViewController()

    researchesDataSource = dependencyContainer
      .dataSourceFactory
      .makeResearchesDataSource(viewController: researchesViewController)

    super.init(nibName: nil, bundle: nil)

    embeddedNavigationController = UINavigationController()
    embeddedNavigationController.navigationBar.prefersLargeTitles = true
    tabBarItem = UITabBarItem(
      title: R.string.localizable.tabBarResearches(),
      image: R.image.tabBar.researches.nonSelected(),
      selectedImage: R.image.tabBar.researches.selected())
    add(childViewController: embeddedNavigationController)
  }

  convenience required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    embeddedNavigationController.viewControllers = [researchesViewController]
    researchesDataSource.render()
  }
}
