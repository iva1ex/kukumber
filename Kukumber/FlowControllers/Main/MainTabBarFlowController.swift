//
//  MainTabBarFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 22.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

protocol MainTabBarFlowControllerDelegate: NSObject {
}

final class MainTabBarFlowController: BaseFlowController {
  weak var delegate: MainTabBarFlowControllerDelegate?
  private let dependencyContainer: MainTabBarDependencyContainer
  private var embeddedTabBarViewController: UITabBarController!

  required init(dependencyContainer: MainTabBarDependencyContainer) {
    self.dependencyContainer = dependencyContainer

    super.init(nibName: nil, bundle: nil)
    embeddedTabBarViewController = UITabBarController()
    add(childViewController: embeddedTabBarViewController)
  }

  convenience required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    let home = dependencyContainer
      .flowControllerFactory
      .makeHomeFlowController(dependencyContainer: dependencyContainer)
    let treatment = dependencyContainer
      .flowControllerFactory
      .makeTreatmentFlowController(dependencyContainer: dependencyContainer)
    let addStub = dependencyContainer
      .viewControllerFactory
      .makeAddStubViewController()
    let researches = dependencyContainer
      .flowControllerFactory
      .makeResearchesFlowController(dependencyContainer: dependencyContainer)
    let settings = dependencyContainer
      .flowControllerFactory
      .makeSettingsFlowController(dependencyContainer: dependencyContainer)

    embeddedTabBarViewController.viewControllers = [home, treatment, addStub, researches, settings]
    embeddedTabBarViewController.selectedViewController = home
    embeddedTabBarViewController.delegate = self
    home.start()
    treatment.start()
    researches.start()
    settings.start()
  }
}

extension MainTabBarFlowController: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    guard viewController is AddStubViewController else { return true }

    let alert = UIAlertController(
      title: R.string.localizable.addTitle(),
      message: nil,
      preferredStyle: .actionSheet)
    alert.addAction(.init(title: R.string.localizable.addOptionDoctorAppointmentTitle(), style: .default, handler: { _ in }))
    alert.addAction(.init(title: R.string.localizable.addOptionVitaminsIntakeTitle(), style: .default, handler: { _ in }))
    alert.addAction(.init(title: R.string.localizable.addOptionNewResearchTitle(), style: .default, handler: { _ in }))
    alert.addAction(.init(title: R.string.localizable.addOptionNewVaccinationTitle(), style: .default, handler: { _ in }))
    alert.addAction(.init(title: R.string.localizable.generalCancel(), style: .cancel, handler: { _ in }))

    embeddedTabBarViewController.present(alert, animated: true)

    return false
  }
}
