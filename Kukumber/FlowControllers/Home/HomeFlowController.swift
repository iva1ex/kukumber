//
//  HomeFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.03.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class HomeFlowController: BaseFlowController {
  let dependencyContainer: HomeDependencyContainer
  private let mainHomeViewController: MainHomeViewController
  private let mainHomeDataSource: MainHomeDataSource
  private let embeddedNavigationController: UINavigationController

  required init(dependencyContainer: HomeDependencyContainer) {
    self.dependencyContainer = dependencyContainer

    mainHomeViewController = dependencyContainer
      .viewControllerFactory
      .makeMainHomeViewController()

    mainHomeDataSource = dependencyContainer
      .dataSourceFactory
      .makeMainHomeDataSource(viewController: mainHomeViewController)

    embeddedNavigationController = UINavigationController()

    super.init(nibName: nil, bundle: nil)

    tabBarItem = UITabBarItem(
      title: R.string.localizable.tabBarHome(),
      image: R.image.tabBar.home.nonSelected(),
      selectedImage: R.image.tabBar.home.selected())
    add(childViewController: embeddedNavigationController)
    mainHomeDataSource.delegate = self
  }

  convenience required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    mainHomeDataSource.render()

    embeddedNavigationController.viewControllers = [mainHomeViewController]
  }
}

// MARK: MainHomeDataSourceDelegate conformance
extension HomeFlowController: MainHomeDataSourceDelegate {
  func didSelectMedication(_ dataSource: MainHomeDataSource, medication: Medication) {
    let medicationFlow = dependencyContainer.flowControllerFactory.makeMedicationDetailsFlowController(
      dependencyContainer: dependencyContainer,
      navigationController: embeddedNavigationController,
      medication: medication)
    medicationFlow.start()
    push(viewController: medicationFlow)
  }

  func didSelectVaccination(_ dataSource: MainHomeDataSource, vaccination: Vaccination) {
    let vaccinationFlow = dependencyContainer.flowControllerFactory.makeVaccinationDetailsFlowController(
      dependencyContainer: dependencyContainer,
      navigationController: embeddedNavigationController,
      vaccination: vaccination)
    vaccinationFlow.start()
    push(viewController: vaccinationFlow)
  }

  func didSelectAlert(_ dataSource: MainHomeDataSource, alert: Alert) {
    let alertFlow = dependencyContainer.flowControllerFactory.makeAlertFlowController(dependencyContainer: dependencyContainer, alert: alert)
    alertFlow.start()
    present(viewController: alertFlow)
  }

  func didDataSourceViewControllerDestroy(_ dataSource: MainHomeDataSource) {
  }

  func didSelectProfile(_ dataSource: MainHomeDataSource) {
    let profileFlow = dependencyContainer.flowControllerFactory.makeProfileFlowController(dependencyContainer: dependencyContainer)
    profileFlow.start()
    present(viewController: profileFlow)
  }
}

// MARK: Present other flow screens
private extension HomeFlowController {
  func push(viewController: UIViewController) {
    embeddedNavigationController.pushViewController(viewController, animated: true)
  }

  func present(viewController: UIViewController) {
    embeddedNavigationController.present(viewController, animated: true)
  }
}
