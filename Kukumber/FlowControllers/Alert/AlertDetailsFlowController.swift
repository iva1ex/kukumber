//
//  AlertDetailsFlowController.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 17.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation
import UIKit

final class AlertDetailsFlowController: BaseFlowController {
  let dependencyContainer: AlertDependencyContainer
  private let viewController: AlertDetailsViewController
  private let dataSource: AlertDataSource

  private let embeddedNavigationController: UINavigationController

  required init(dependencyContainer: AlertDependencyContainer, alert: Alert) {
    self.dependencyContainer = dependencyContainer
    self.embeddedNavigationController = UINavigationController()

    self.viewController = dependencyContainer
      .viewControllerFactory
      .makeAlertDetailsViewController()

    self.dataSource = dependencyContainer
      .dataSourceFactory
      .makeAlertDetailsDataSource(viewController: viewController)

    super.init(nibName: nil, bundle: nil)
    navigationItem.largeTitleDisplayMode = .never
    navigationItem.title = R.string.localizable.alertTitle()

    add(childViewController: embeddedNavigationController)
    dataSource.delegate = self
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    dataSource.render()
    embeddedNavigationController.viewControllers = [viewController]
  }
}

extension AlertDetailsFlowController: AlertDataSourceDelegate {
  func didDataSourceViewControllerDestroy(_ dataSource: AlertDataSource) {
  }

  func didGoBack(_ dataSource: AlertDataSource) {
    dismiss(animated: true)
  }
}
