//
//  ResearchesDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 22.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol ResearchesDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: ResearchesDataSource)
}

final class ResearchesDataSource {
  weak var delegate: ResearchesDataSourceDelegate?

  weak var viewController: ResearchesViewController?

  required init(viewController: ResearchesViewController) {
    self.viewController = viewController

    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    let testResearches = Research.Test.multiple
    render(researches: testResearches)
  }
}

extension ResearchesDataSource {
  func render(researches: [Research]) {
    let props = ResearchesViewController.Props(
      title: R.string.localizable.researchesTitle(),
      editButton: .init(
        title: R.string.localizable.generalEdit(),
        action: Command { print("Редактировать архив") }),
      researches: researches.map { research in
        .init(
          name: research.name,
          date: research.date,
          delete: Command { [weak self] in
            self?.render(researches: researches.filter { $0.id != research.id})
          }
        )
      }
    )
    renderProps(props)
  }

  func renderProps(_ props: ResearchesViewController.Props) {
    viewController?.render(props)
  }
}
