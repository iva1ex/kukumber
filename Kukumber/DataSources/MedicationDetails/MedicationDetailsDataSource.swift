//
//  MedicationDetailsDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 29.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol MedicationDetailsDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: MedicationDetailsDataSource)
}

final class MedicationDetailsDataSource {
  weak var delegate: MedicationDetailsDataSourceDelegate?

  weak var viewController: MedicationDetailsViewController?
  private let medication: Medication

  required init(
    viewController: MedicationDetailsViewController,
    medication: Medication) {
    self.viewController = viewController
    self.medication = medication
    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    viewController?.render(MedicationDetailsViewController.Props.init(title: medication.name))
  }
}

private extension MedicationDetailsDataSource {
}
