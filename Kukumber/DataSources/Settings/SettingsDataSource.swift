//
//  SettingsDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 09.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol SettingsDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: SettingsDataSource)
}

final class SettingsDataSource {
  weak var delegate: SettingsDataSourceDelegate?

  weak var viewController: SettingsViewController?

  required init(viewController: SettingsViewController) {
    self.viewController = viewController
    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    let props = SettingsViewController.Props.init(
      title: R.string.localizable.settingsTitle(),
      sections: [
        .init(rows: [
          .init(
            image: R.image.settings.appVersion(),
            title: R.string.localizable.settingsAppVersionTitle(),
            content: .detailedText(text: "1.0"))
          ]
        ),
        .init(rows: [
          .init(
            image: R.image.settings.notifications(),
            title: R.string.localizable.settingsNotificationsTitle(),
            content: .switchItem(isOn: false, update: CommandWith { newValue in
              print("Notifications Switch change to: \(newValue)")
              }
            )
          ),
          .init(
            image: R.image.settings.darkMode(),
            title: R.string.localizable.settingsDarkModeTitle(),
            content: .switchItem(isOn: false, update: CommandWith { newValue in
              print("Dark Mode Switch change to: \(newValue)")
              }
            )
          ),
          .init(
            image: R.image.settings.faceId(),
            title: R.string.localizable.settingsAuthenticationTitle(),
            content: .switchItem(isOn: false, update: CommandWith { newValue in
              print("Authentication Switch change to: \(newValue)")
              }
            )
          )
          ]
        ),
        .init(rows: [
          .init(
            image: R.image.settings.privacy(),
            title: R.string.localizable.settingsPrivacyTitle(),
            content: .none),
          .init(
            image: R.image.settings.contactSupport(),
            title: R.string.localizable.settingsContactSupportTitle(),
            content: .none)
          ]
        ),
        .init(rows: [
          .init(
            image: R.image.settings.rateUs(),
            title: R.string.localizable.settingsRateUsTitle(),
            content: .none)
          ]
        )
      ]
    )
    viewController?.render(props)
  }
}

private extension SettingsDataSource {
}
