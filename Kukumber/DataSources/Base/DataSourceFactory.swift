//
//  DataSourceFactory.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 05.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol DataSourceFactoryProtocol: NSObject {
  func makeMainHomeDataSource(viewController: MainHomeViewController) -> MainHomeDataSource
  func makeTreatmentDataSource(viewController: TreatmentViewController) -> TreatmentDataSource
  func makePreventionDataSource(viewController: PreventionViewController) -> PreventionDataSource
  func makeResearchesDataSource(viewController: ResearchesViewController) -> ResearchesDataSource
  func makeMedicationDetailsDataSource(
    viewController: MedicationDetailsViewController,
    medication: Medication) -> MedicationDetailsDataSource
  func makeVaccinationDetailsDataSource(
    viewController: VaccinationDetailsViewController,
    vaccination: Vaccination) -> VaccinationDetailsDataSource
  func makeProfileDataSource(viewController: ProfileViewController) -> ProfileDataSource
  func makeSettingsDataSource(viewController: SettingsViewController) -> SettingsDataSource
  func makeAlertDetailsDataSource(viewController: AlertDetailsViewController) -> AlertDataSource
}

final class DataSourceFactory: NSObject, DataSourceFactoryProtocol {
  func makeMainHomeDataSource(viewController: MainHomeViewController) -> MainHomeDataSource {
    return MainHomeDataSource(viewController: viewController)
  }

  func makeTreatmentDataSource(viewController: TreatmentViewController) -> TreatmentDataSource {
    return TreatmentDataSource(viewController: viewController)
  }

  func makePreventionDataSource(viewController: PreventionViewController) -> PreventionDataSource {
    return PreventionDataSource(viewController: viewController)
  }

  func makeResearchesDataSource(viewController: ResearchesViewController) -> ResearchesDataSource {
    return ResearchesDataSource(viewController: viewController)
  }

  func makeMedicationDetailsDataSource(
    viewController: MedicationDetailsViewController,
    medication: Medication) -> MedicationDetailsDataSource {
    return MedicationDetailsDataSource(viewController: viewController, medication: medication)
  }

  func makeVaccinationDetailsDataSource(
  viewController: VaccinationDetailsViewController,
  vaccination: Vaccination) -> VaccinationDetailsDataSource {
    return VaccinationDetailsDataSource(viewController: viewController, vaccination: vaccination)
  }

  func makeProfileDataSource(viewController: ProfileViewController) -> ProfileDataSource {
    return ProfileDataSource(viewController: viewController)
  }

  func makeSettingsDataSource(viewController: SettingsViewController) -> SettingsDataSource {
    return SettingsDataSource(viewController: viewController)
  }

  func makeAlertDetailsDataSource(viewController: AlertDetailsViewController) -> AlertDataSource {
    return AlertDataSource(viewController: viewController)
  }
}
