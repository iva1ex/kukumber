//
//  ProfileDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 03.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol ProfileDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: ProfileDataSource)
  func didGoBack(_ dataSource: ProfileDataSource)
}

final class ProfileDataSource {
  weak var delegate: ProfileDataSourceDelegate?

  weak var viewController: ProfileViewController?

  required init(viewController: ProfileViewController) {
    self.viewController = viewController
    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    let props = ProfileViewController.Props.init(
      title: R.string.localizable.profileTitle(),
      back: Command { [weak self] in
        guard let self = self else { return }
        self.delegate?.didGoBack(self)
      }
    )
    viewController?.render(props)
  }
}

private extension ProfileDataSource {
}
