//
//  AlertDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 17.05.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol AlertDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: AlertDataSource)
  func didGoBack(_ dataSource: AlertDataSource)
}

final class AlertDataSource {
  weak var delegate: AlertDataSourceDelegate?

  weak var viewController: AlertDetailsViewController?

  required init(viewController: AlertDetailsViewController) {
    self.viewController = viewController
    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    let props = AlertDetailsViewController.Props.init(
      title: R.string.localizable.alertTitle(),
      back: Command { [weak self] in
        guard let self = self else { return }
        self.delegate?.didGoBack(self)
      }
    )
    viewController?.render(props)
  }
}

private extension AlertDataSource {
}
