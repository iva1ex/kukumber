//
//  MainHomeDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 05.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol MainHomeDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: MainHomeDataSource)
  func didSelectMedication(_ dataSource: MainHomeDataSource, medication: Medication)
  func didSelectVaccination(_ dataSource: MainHomeDataSource, vaccination: Vaccination)
  func didSelectAlert(_ dataSource: MainHomeDataSource, alert: Alert)
  func didSelectProfile(_ dataSource: MainHomeDataSource)
}

final class MainHomeDataSource {
  weak var delegate: MainHomeDataSourceDelegate?

  weak var viewController: MainHomeViewController?

  required init(viewController: MainHomeViewController) {
    self.viewController = viewController

    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    let testMedications = Medication.Test.multiple
    let testVaccinations = Vaccination.Test.multiple

    render(medications: testMedications, vaccinations: testVaccinations)
  }
}

// swiftlint:disable function_body_length
private extension MainHomeDataSource {
  func render(
    medications: [Medication],
    vaccinations: [Vaccination]) {
    let props = MainHomeViewController.Props(
      title: R.string.localizable.homeTitle(),
      alerts: Alert.Test.multiple.map { alert in
        .init(text: alert.text, select: Command { [weak self] in
          guard let self = self else { return }
          self.delegate?.didSelectAlert(self, alert: alert)
        }
      )
      },
      openProfile: Command { [weak self] in
        guard let self = self else { return }
        self.delegate?.didSelectProfile(self)
      },
      medicationSection: .init(
        title: R.string.localizable.homeMedicationSectionTitle(),
        allButton: .init(
          title: R.string.localizable.generalAll(),
          action: Command { print("Показать все медикаменты") }),
        medications: medications.map { medication in
          .init(
            name: medication.name,
            amountText: medication.amountText,
            time: medication.time,
            delete: Command { [weak self] in
              self?.render(
                medications: medications.filter { $0.id != medication.id },
                vaccinations: vaccinations)
            },
            select: Command { [weak self] in
              guard let self = self else { return }
              self.delegate?.didSelectMedication(self, medication: medication)
            }
          )
      }),
      vaccinationSection: .init(
        title: R.string.localizable.homeVaccinationSectionTitle(),
        allButton: .init(
          title: R.string.localizable.generalAll(),
          action: Command { print("Показать все медикаменты") }),
        vaccinations: vaccinations.map { vaccination in
          .init(
            name: vaccination.name,
            date: vaccination.date,
            delete: Command { [weak self] in
              self?.render(
              medications: medications,
              vaccinations: vaccinations.filter { $0.id != vaccination.id })
            },
            select: Command { [weak self] in
              guard let self = self else { return }
              self.delegate?.didSelectVaccination(self, vaccination: vaccination)
            }
          )
      }))
    renderProps(props)
  }

  func renderProps(_ props: MainHomeViewController.Props) {
    viewController?.render(props)
  }
}
