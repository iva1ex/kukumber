//
//  PreventionDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 20.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol PreventionDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: PreventionDataSource)
}

final class PreventionDataSource {
  weak var delegate: PreventionDataSourceDelegate?

  weak var viewController: PreventionViewController?

  required init(viewController: PreventionViewController) {
    self.viewController = viewController

    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    viewController?.render(PreventionViewController.Props.init())
  }
}
