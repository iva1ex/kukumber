//
//  TreatmentDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 20.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol TreatmentDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: TreatmentDataSource)
}

final class TreatmentDataSource {
  weak var delegate: TreatmentDataSourceDelegate?

  weak var viewController: TreatmentViewController?

  required init(viewController: TreatmentViewController) {
    self.viewController = viewController

    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    let testCurrentTreatments = Treatment.Test.multipleCurrent
    let testArchivedTreatments = Treatment.Test.multipleArchived
    render(currentTreatments: testCurrentTreatments, archivedTreatments: testArchivedTreatments)
  }
}

extension TreatmentDataSource {
  func render(
    currentTreatments: [Treatment],
    archivedTreatments: [Treatment]) {
    let props = TreatmentViewController.Props(
    currentTreatmentSection: .init(
      title: R.string.localizable.treatmentTreatmentTabCurrentSectionTitle(),
      allButton: .init(
        title: R.string.localizable.generalAll(),
        action: Command { print("Показать все текущее лечение") }),
      treatments: currentTreatments.map { currentTreatment in
        .init(
          name: currentTreatment.name,
          dateRange: currentTreatment.dateRange,
          delete: Command { [weak self] in
            self?.render(
              currentTreatments: currentTreatments.filter { $0.id != currentTreatment.id },
              archivedTreatments: archivedTreatments)
          }
        )
    }),
    archiveTreatmentSection: .init(
      title: R.string.localizable.treatmentTreatmentTabArchiveSectionTitle(),
      allButton: .init(
        title: R.string.localizable.generalAll(),
        action: Command { print("Показать все архивное лечение") }),
      treatments: archivedTreatments.map { archivedTreatment in
        .init(
          name: archivedTreatment.name,
          dateRange: archivedTreatment.dateRange,
          delete: Command { [weak self] in
            self?.render(
              currentTreatments: currentTreatments,
              archivedTreatments: archivedTreatments.filter { $0.id != archivedTreatment.id })
        })
      }
      ))

    renderProps(props)
  }

  func renderProps(_ props: TreatmentViewController.Props) {
    viewController?.render(props)
  }
}
