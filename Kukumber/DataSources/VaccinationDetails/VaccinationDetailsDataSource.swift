//
//  VaccinationDetailsDataSource.swift
//  Kukumber
//
//  Created by Alexander Ivashchenko on 30.04.2020.
//  Copyright © 2020 Kukumber Company. All rights reserved.
//

import Foundation

protocol VaccinationDetailsDataSourceDelegate: class {
  func didDataSourceViewControllerDestroy(_ dataSource: VaccinationDetailsDataSource)
}

final class VaccinationDetailsDataSource {
  weak var delegate: VaccinationDetailsDataSourceDelegate?

  weak var viewController: VaccinationDetailsViewController?
  private let vaccination: Vaccination

  required init(
    viewController: VaccinationDetailsViewController,
    vaccination: Vaccination) {
    self.viewController = viewController
    self.vaccination = vaccination
    ObjectDeinitNotifier.notify(for: viewController) {
      self.delegate?.didDataSourceViewControllerDestroy(self)
    }
  }

  func render() {
    viewController?.render(VaccinationDetailsViewController.Props.init(title: vaccination.name))
  }
}

private extension VaccinationDetailsDataSource {
}
